/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitestemporalisees;

import ModelEvent.Model;

/**
 *
 * @author Thibault
 */
public abstract class AbstractEntiteTemporalisee extends Model implements EntitesTemporalisees {

    protected long tempsCourant;
    protected long tempsDebut;
    protected long tempsFin;

    public AbstractEntiteTemporalisee(long tempsDebut, long tempsFin) {
        this.tempsCourant = tempsDebut;
        this.tempsDebut = tempsDebut;
        this.tempsFin = tempsFin;
    }

    @Override
    public long getTempsDebut() {
        return this.tempsDebut;
    }

    @Override
    public long getTempsFin() {
        return this.tempsFin;
    }

    @Override
    public void setTemps(long temps) {
        this.tempsCourant = temps;
        maj();
        
    }
    
    abstract protected void maj();

}
