/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitestemporalisees;

import java.util.ArrayList;
import java.util.Observable;

/**
 *
 * @author Thibault
 */
public class Temps extends Observable {

    protected long tempsCourant;
    protected long tempsDebut;
    protected long tempsFin;
    protected ArrayList<EntitesTemporalisees> entites;

    public Temps() {
        this.entites = new ArrayList();
        this.tempsDebut = -1;
    }

    protected void addEntite(EntitesTemporalisees entite) {
        this.entites.add(entite);
        this.updateTimeFrameAdd(entite);
    }

    protected void removeEntite(EntitesTemporalisees entite) {
        this.entites.remove(entite);
        this.updateTimeFrameRm(entite);
    }

    protected void setTemps() {
        for (EntitesTemporalisees entite : entites) {
            entite.setTemps(this.tempsCourant);
        }
    }

    protected void start() {
        while (true) {
            sleep();
            tempsCourant++;
            this.setTemps();
        }
    }

    private void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    private void updateTimeFrameAdd(EntitesTemporalisees entite) {
        if (this.tempsDebut == -1) {
            this.tempsDebut = entite.getTempsDebut();
            this.tempsFin = entite.getTempsFin();
            this.tempsCourant = entite.getTempsDebut();
            return;
        }
        if (entite.getTempsDebut() < this.tempsDebut) {
            this.tempsDebut = entite.getTempsDebut();
        }
        if (entite.getTempsFin() > this.tempsFin) {
            this.tempsFin = entite.getTempsFin();
        }
    }

    private void updateTimeFrameRm(EntitesTemporalisees entite) {
        if (!isInsideTimeFrame(entite)) {
            initializeTemps();
            for (EntitesTemporalisees e : entites) {
                if (entite.getTempsDebut() < this.tempsDebut) {
                    this.tempsDebut = entite.getTempsDebut();
                }
                if (entite.getTempsFin() > this.tempsFin) {
                    this.tempsFin = entite.getTempsFin();
                }
            }
        }
    }

    private void initializeTemps() {
        this.tempsDebut = 0;
        this.tempsFin = 0;
    }

    private Boolean isInsideTimeFrame(EntitesTemporalisees entite) {
        if ((entite.getTempsDebut() <= this.tempsDebut) || (entite.getTempsFin() >= this.tempsFin)) {
            return false;
        }
        return true;
    }
}
