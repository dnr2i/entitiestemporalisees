/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitestemporalisees.chien;

/**
 *
 * @author Thibault
 */
public class ActionChien {

    protected long tempsDebut;
    protected long duree;
    protected String label;

    public ActionChien(long tempsDebut, long duree, String label) {
        this.tempsDebut = tempsDebut;
        this.duree = duree;
        this.label = label;
    }

    @Override
    public String toString() {
        return label + " " + tempsDebut + " " + duree;
    }

    /**
     *
     * @return
     */
    public String getLabel() {
        return this.label;
    }

    public long getTempsDebut() {
        return this.tempsDebut;
    }

    public long getTempsFin() {
        return this.tempsDebut + this.duree;
    }

}
