/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitestemporalisees.chien;

import ModelEvent.ModelInterface;
import ModelEvent.ModeleListener;
import entitestemporalisees.AbstractEntiteTemporalisee;
import java.util.ArrayList;

/**
 *
 * @author Thibault
 */
public class Chien extends AbstractEntiteTemporalisee {

    protected ArrayList<ActionChien> actions;
    protected ArrayList<ActionChien> actionsEnCours;
    protected ArrayList<ModeleListener> modelListener;

    public Chien(long tempsDebut, long tempsFin) {
        super(tempsDebut, tempsFin);
        this.actions = new ArrayList();
    }

    public void addAction(ActionChien action) {
        this.actions.add(action);
        this.maj();
    }

    /**
     *
     */
    @Override
    public void maj() {
        this.actionsEnCours = new ArrayList();
        for (ActionChien actionChien : actions) {
            if (actionChien.getTempsDebut() <= this.tempsCourant && actionChien.getTempsFin() >= this.tempsCourant) {
                this.actionsEnCours.add(actionChien);
            }
        }
        this.fireChange();
    }

    public ArrayList<ActionChien> getActionsEnCours() {
        return actionsEnCours;
    }

}
