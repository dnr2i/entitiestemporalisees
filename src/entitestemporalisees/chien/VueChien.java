package entitestemporalisees.chien;

import ModelEvent.ModelInterface;
import ModelEvent.ModeleListener;
import java.util.ArrayList;
import javax.swing.JList;

/**
 *
 * @author 21205282
 */
public class VueChien extends JList implements ModeleListener {

    Chien chien;

    public VueChien(Chien chien) {
        this.chien = chien;
        chien.addModelListener(this);
    }

    @Override
    public void modelChanged(ModelInterface m) {
        Chien chien = (Chien) m;
        ArrayList<ActionChien> actionsEnCours = chien.getActionsEnCours();
        String[] actionsLabels = new String[actionsEnCours.size()];
        for (int i = 0; i < actionsEnCours.size(); i++) {
            actionsLabels[i] = actionsEnCours.get(i).getLabel();
        }
        this.setListData(actionsLabels);
    }

}
