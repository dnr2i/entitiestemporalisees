/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitestemporalisees;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author nicolasv
 */
public class TempsControleur extends JPanel implements Observer, ChangeListener {

    protected Dimension dim;
    /* TEMPORAIRE, JUSTE POUR AVOIR UN APERCU DU SLIDER */
    protected int FPS_MIN = 0;
    protected int FPS_MAX = 30;
    protected int FPS_INIT = 15;
    
    protected JLabel debut = new JLabel("0");
    protected JLabel fin = new JLabel("100");
    protected JLabel current = new JLabel("50");
    
    protected JButton reset = new JButton("Reset");
    protected JButton end = new JButton("End");
    
    JPanel panelScene = new JPanel();
    
    protected JCheckBox synchronize = new JCheckBox("Synchronisation");
    
    

    public TempsControleur(Temps tps) {
        tps.addObserver(this);
        //dim = new Dimension(screenWidth, screenHeight);
        //setPreferredSize(dim);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    
        initTitle();
        initScene();
        initJTextfield();
        initSlider();
        initButton();
        initCheckBox();
    }

    private void initTitle() {
        JPanel panelTextSlider = new JPanel();
        JLabel sliderLabel = new JLabel("Time Slider", JLabel.CENTER);
        panelTextSlider.add(sliderLabel);
        add(panelTextSlider);
    }
    
    private void initScene() {
        panelScene.setBorder(BorderFactory.createLineBorder(Color.black));
        panelScene.setMinimumSize(new Dimension(300, 300));
        panelScene.setPreferredSize(new Dimension(300, 300));
        add(panelScene);
    }

    private void initJTextfield() {
        JPanel panelTimeLabels = new JPanel();
        panelTimeLabels.setLayout(new BorderLayout());
        //panelTimeLabels.setMinimumSize(new Dimension(screenWidth, 0));
        panelTimeLabels.add(debut, BorderLayout.WEST);
        current.setHorizontalAlignment(JLabel.CENTER);
        panelTimeLabels.add(current, BorderLayout.CENTER);
        panelTimeLabels.add(fin, BorderLayout.EAST);
        
//        panelTimeLabels.setBorder(BorderFactory.createLineBorder(Color.pink));
//        debut.setBorder(BorderFactory.createLineBorder(Color.black));
//        current.setBorder(BorderFactory.createLineBorder(Color.BLUE));
//        fin.setBorder(BorderFactory.createLineBorder(Color.red));
        
        add(panelTimeLabels);
        System.out.println(panelTimeLabels.getWidth());
    }
    
    private void initSlider() {
        JSlider timeSlider = new JSlider(JSlider.HORIZONTAL, FPS_MIN, FPS_MAX, FPS_INIT);
        timeSlider.addChangeListener(this);
        setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        
        add(timeSlider);
    }
    
    public void initButton(){
        
        JPanel buttonPanel = new JPanel(new BorderLayout());
        //buttonPanel.setMaximumSize(new Dimension(screenWidth, 10));
        buttonPanel.add(reset, BorderLayout.WEST);
        buttonPanel.add(end, BorderLayout.EAST);
        
        add(buttonPanel);
        
    }
    
    public void initCheckBox(){
        JPanel checkBoxPanel = new JPanel();
        checkBoxPanel.add(synchronize);
        add(checkBoxPanel);
        
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println(o);
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        System.out.println(e);
    }

}
