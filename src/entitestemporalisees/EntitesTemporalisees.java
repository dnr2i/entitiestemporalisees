/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitestemporalisees;

/**
 *
 * @author Thibault
 */
public interface EntitesTemporalisees {

    public long getTempsDebut();

    public long getTempsFin();

    public void setTemps(long temps);
}
