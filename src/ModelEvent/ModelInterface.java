/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelEvent;

/**
 *
 * @author 21205282
 */
public interface ModelInterface {

    /**
     *
     * @param m
     */
    abstract void addModelListener(ModeleListener m);

    /**
     *
     * @param m
     */
    abstract void removeModelListener(ModeleListener m);
    
}
