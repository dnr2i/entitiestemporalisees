/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ModelEvent;

import java.util.ArrayList;

/**
 *
 * @author 21205282
 */
public class Model implements ModelInterface {

    ArrayList<ModeleListener> modelListener;

    public Model() {        
        this.modelListener = new ArrayList<>();
    }

    @Override
    public void addModelListener(ModeleListener m) {
        modelListener.add(m);
    }

    @Override
    public void removeModelListener(ModeleListener m) {
        modelListener.remove(m);
    }

    public void fireChange() {
        for (ModeleListener l : modelListener) {
            l.modelChanged(this);
        }
    }
}
